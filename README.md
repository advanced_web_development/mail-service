# Environment setup

## 1. Install all node modules.

Open the terminal in the source code folder and run the following command to install all the necessary node modules:

“npm i”

## 2. Create .env file

The source code requires a .env file to store secret data. Create a .env file in the source code folder with the following content:

SEND_GRID_API=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
MAIL_CONFIRMATION_URL=http://localhost:3000/auth/jwt/confirm/redirect
REDIS_CLUSTER_URL=redis://default:eYVX7EwVmmxKPCDmwMtyKVge8oLd2t81@localhost:6379
SENDER_ACCOUNT=20120529@student.hcmus.edu.vn

Note 1: Please use the same redis queue as the auth service task queue
Note 2: Please create your SEND GRID ACCOUNT and then use your API KEY and SENDER ACCOUNT
Note 3: You just need to run the worker and then it will automatically execute mail tasks when ever there is any tasks in the queue.
Note 4: You can use our deployed redis instead "redis://default:O9Dr2y87Sv2cYf61WOVZCvvNTWqdq2Gb@redis-11353.c295.ap-southeast-1-1.ec2.redns.redis-cloud.com:11353"

## 3. Start the project

Run “npx nest start” to start the project.
