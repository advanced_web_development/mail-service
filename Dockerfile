FROM node:20-alpine3.17 
# Create app directory
WORKDIR /usr/src/app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

# Install app dependencies
RUN npm install


# Bundle app source
COPY . .
# Creates a "dist" folder with the production build
# RUN cat .env
RUN npm run build

# FROM node:20
# COPY --from=builder /usr/src/app/node_modules ./node_modules
# COPY --from=builder /usr/src/app/package*.json ./
# COPY --from=builder /usr/src/app/dist ./dist
# Start the server using the production build
CMD [ "node", "dist/main.js"  ]
