import { BullModule } from '@nestjs/bull';
import { Module } from '@nestjs/common';
import { MailProviderModule } from 'src/mail_provider/mail_provider.module';
import { InvitationProcessor } from './invitation.processor';

@Module({
  imports: [
    BullModule.registerQueue({
      name: 'class-invitation',
    }),
    MailProviderModule,
    InvitationModule,
  ],
  providers: [InvitationProcessor],
})
export class InvitationModule {}
