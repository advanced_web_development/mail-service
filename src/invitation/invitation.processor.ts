import {
  OnQueueActive,
  OnQueueCompleted,
  OnQueueRemoved,
  Process,
  Processor,
} from '@nestjs/bull';
import { Logger } from '@nestjs/common';
import { Job } from 'bull';
import { MailProviderService } from 'src/mail_provider/mail_provider.service';

@Processor('class-invitation')
export class InvitationProcessor {
  constructor(private mailProvider: MailProviderService) {}
  private readonly logger = new Logger(InvitationProcessor.name);
  @OnQueueActive()
  onActice(job: Job) {
    console.log(`Processing job ${job.id} of type ${job.name}`);
  }

  @OnQueueCompleted()
  onCompleted(job: Job) {
    job.remove();
  }

  @OnQueueRemoved()
  onRemove(job: Job) {
    console.log(`remove job id:${job.id}`);
  }
  @Process('student_invitation')
  async handleMailConfirmation(job: Job) {
    this.logger.debug('Start sending mail confirmation');
    this.logger.debug(job.data);
    console.log(job.data);
    const token: string = job.data.token;
    const email: string = job.data.email;
    const role: string = job.data.role;
    // console.log(token, mail);
    const url: string = job.data.url;
    await this.mailProvider.classInvitation(role, token, url, email);
    // await this.mailProvider.confirmMail(mail, token);
    return 'OK';
  }
}
