import * as dotenv from 'dotenv';
dotenv.config();
class Config {
  SEND_GRID_API = process.env.SEND_GRID_API;
  MAIL_CONFIRMATION_URL = process.env.MAIL_CONFIRMATION_URL;
  MAIL_QUEUE_URL = process.env.MAIL_QUEUE_URL;
  SENDER_ACCOUNT = process.env.SENDER_ACCOUNT;
}

export const _config = new Config();
console.log(_config.MAIL_QUEUE_URL);
